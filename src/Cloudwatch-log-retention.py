################################################################################
#                                                                              #                
#  Author           : Dipayan Ganguly                                          #                          
#  Application Name : cargoCloudwatchRetention                                 #
#  File Name        : Cloudwatch-log-retention.py                              #
#  Business Flow    : Cloudwatch log retention set to delete unnecessary logs  #
#                     after a certain time                                     #
#  Creation Date    : 30-08-2021                                               #
#                                                                              #
################################################################################
# Date Change by Description                                                   #
# 1 30-08-2021 Dipayan Ganguly : DevOps                                        #                                      
#                                                                              #
#                                                                              #
#                                                                              #
################################################################################



import json
import boto3
import os
import logging
def lambda_handler(event, context):
    
    #Storing environment variable
    regions= [os.environ['AWS_REGION']]
    SKIP_LOG_GROUP = os.environ['SKIP_LOG_GROUP']
    
    Store "retention_to" input which is being taken from cloudwatch events rules.
    retention_days = event['retention_to']
    
    #Set logger.setlevel to logging.INFO, so that it can handle only INFO and ERROR messages
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    count1 = 0
    count2 = 0
    
    for region in regions:
        client = boto3.client('logs',region)
        try:
            response = client.describe_log_groups()
        except Exception as e:
            logger.error(e)
            raise e
        try:
            nextToken=response.get('nextToken',None)
        except Exception as e:
            logger.error(e)
            raise e
        retention = response['logGroups']
        while (nextToken is not None):
            response = client.describe_log_groups(
                nextToken=nextToken
            )
            nextToken = response.get('nextToken', None)
            retention = retention + response['logGroups']
            count1 = count1 + 1
        count = 0
        # Check if there is any log group(s) which we need to skip from setting retention 
        # If yes then the log group names to be read from "SkipLogGroup.txt" file.
        for group in retention:
            if retentionInDays in group.keys():
                count2 = count2 + 1
        if count1 == count2:
            logger.info("No log groups found with retention days : never expire ")
                
        excludeLogGroups= []
        if SKIP_LOG_GROUP == 'true':
            try:
                file = open("SkipLogGroup.txt", "r")
            except Exception as e:
                logger.error(e)
                raise e
            logs = file.readlines()
            for x in logs:
                excludeLogGroups.append(x.replace("\n", ""))
        #Take 400 log groups within a lambda execution and setting retention for those log groups.
        #After one lambda execution, it will execute next 400 log groups within next execution.
        #This process will continue until all log group's retention days are set.
        try:
            for group in retention:
                #Skip those log groups for which retention are already set.
                if 'retentionInDays' in group.keys():
                    logger.info("Retention days for {} is already set-> This is skipped".format(group['logGroupName']))
                    continue
                #Skip those log groups which are mentioned in "SkipLogGroup.txt" file
                elif group['logGroupName'] in excludeLogGroups:
                    logger.info("{} -> is skipped".format(group['logGroupName']))
                    continue
                #Check if the count(starting from 1 which is >0) for log groups has reached at any multiple of 400. eg.- 400,800,1200.
                #If yes then set retention for that log group and breaking the loop.
                elif count> 0 and count%400 == 0:
                    try:
                        setretention = client.put_retention_policy(
                            logGroupName=group['logGroupName'],
                            retentionInDays=retention_days
                        )
                    except Exception as e:
                        logger.error(e)
                        raise e
                    logger.info("{} -> retention day is set to -> {}".format(group['logGroupName'], retention_days))
                    break
                #Set retention for other 399 log groups at a time.
                else :
                    try:
                        setretention = client.put_retention_policy(
                            logGroupName=group['logGroupName'],
                            retentionInDays=retention_days
                        )
                    except Exception as e:
                        logger.error(e)
                        raise e
                    logger.info("{} -> retention day is set to -> {}".format(group['logGroupName'], retention_days))
                    count = count + 1
        except Exception as e:
            logger.error(e)
            raise e
                
    return {
        'statusCode': 200,
        'body': json.dumps({'from': event['retention_from'], 'to': event['retention_to']})
    }